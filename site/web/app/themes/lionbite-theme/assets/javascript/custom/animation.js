function removeLock () {
  $( "body" ).removeClass( "locked" );
}

var isOpen = false;

function stopBodyScrolling (bool) {
    if (bool === true) {
        document.body.addEventListener("touchmove", freezeVp, false);
    } else {
        document.body.removeEventListener("touchmove", freezeVp, false);
    }
}

var freezeVp = function(e) {
    e.preventDefault();
};

function hamburgerEvent(){
  if (isOpen === false) {
  	$( ".site-header" ).addClass( "open" );
  	$( "#logo" ).addClass( "open" );
  	$( "#nav-icon3" ).addClass( "open" );
  	$( "body" ).addClass( "locked" );
    stopBodyScrolling (true);
    isOpen = true;
  } else if (isOpen === true) {
    $( ".site-header" ).removeClass( "open" );
    $( "#logo" ).removeClass( "open" );
    $( "#nav-icon3" ).removeClass( "open" );
    $( "body" ).removeClass( "locked" );
    stopBodyScrolling (false);
    isOpen = false;
  }
}


$(".site-header").swipe({
  swipeDown:function(event, direction, distance, duration, fingerCount) {
    $( ".site-header" ).removeClass( "open" );
    $( ".h-container" ).removeClass( "open" );
    $( "#logo" ).removeClass( "open" );
    $( "#nav-icon3" ).removeClass( "open" );
    stopBodyScrolling (false);
    setTimeout(function() {$( "body" ).removeClass( "locked" );},1000);
    isOpen = false;
  }
});


var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
var player;
function onYouTubeIframeAPIReady() {
  player = new YT.Player('player', {
    height: '1400',
    width: '810',
    videoId: 'Q7axU-PbZuY',
    playerVars: {  'controls': 2, 'rel': 0, 'modestbranding': 1, 'showinfo': 0 },
    events: {
      'onStateChange': onPlayerStateChange
    }
  });
}

$( window ).on( "load", function() {
  if($('body').is('.page-id-201')){
  $(".intro").velocity("transition.fadeIn", { easing: "easeOutSine", duration: 2000, delay: 1000 } );
  $(".module--page-header").velocity("transition.fadeIn", { easing: "easeOutSine", duration: 2000, delay: 0 } );
  $(".module--intro, .module--content, .module--newsletter, .down").velocity("transition.fadeIn", { easing: "easeOutSine", duration: 2000, delay: 2000 } );
  } else if ($('body').is('.page-id-246')){
    $('input#hidden').attr('value', 'Press');
    $('input#email').attr('placeholder', 'Email address (press)');
  }
});

$("#play").click(function() {
  $(".content, .bg").velocity("transition.shrinkOut", { duration: 250 });
  $(".slideshow").velocity("transition.fadeOut", { duration: 250 });
  player.playVideo();
});
$(".down").click(function() {
    $('html, body').animate({
        scrollTop: $(".module--intro").offset().top
    }, 1000);
});

function onPlayerStateChange(event) {
  if($(window).width() >= 640){
    if (event.data == YT.PlayerState.PLAYING) {
      $('#masthead').velocity("transition.shrinkOut", { duration: 100 });
    } else if (event.data == YT.PlayerState.PAUSED) {
      $('#masthead').velocity("transition.shrinkIn", { duration: 250 });
    } else if (event.data == YT.PlayerState.ENDED) {
      $('#masthead').velocity("transition.shrinkIn", { duration: 250 });
      $(".content, .bg, .slideshow").velocity("transition.fadeIn", { duration: 250 });
    }
  } else {
    if (event.data == YT.PlayerState.ENDED || event.data == YT.PlayerState.PAUSED) {
      $(".flexvideo").css("height", "65vh");
      $(".content, .bg, .slideshow").velocity("transition.fadeIn", { duration: 250 });
    }
  }
}