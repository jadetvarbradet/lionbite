<?php
$heroBg = wp_get_attachment_image_src(get_sub_field('image'), 'full');
$heroLogo = wp_get_attachment_image_src(get_sub_field('figure_image'), 'full');
?>

<section class="module--image">
	<div class="background" style="background: url(<?php echo $heroBg[0] ?>); background-size: cover; background-position: center;">
	</div>
	<div class="container">
		<figure>
			<img id="logo" src="<?php echo $heroLogo[0]?>">
			<h1><?php the_sub_field('tagline') ?></h1>
		</figure>
	</div>
</section>