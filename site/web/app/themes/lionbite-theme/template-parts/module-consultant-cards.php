<section class="module--consultant-cards">
	<div class="section-content">
		<div class="grid row small-up-1 medium-up-2 large-up-3">
			<?php
			if( have_rows('card') ):
			    while ( have_rows('card') ) : the_row(); ?>
				<div class="column card-wrap">
					<div class="consultant-card">
						<header>
							<?php $image = wp_get_attachment_image_src(get_sub_field('icon'), 'lionbite-icon'); ?>
							<img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(get_sub_field('icon')) ?>" />
						</header>
						<div class="content">
							<?php if( get_sub_field('title') ): ?>
							<h5 class="dot-title"><?php the_sub_field('title'); ?></h5>
							<?php endif; ?>
							<?php the_sub_field('content'); ?>
			        	</div>
			        </div>
			    </div>
			    <?php endwhile;
			else :
			endif;
			?>
		</div>
	</div>
</section>