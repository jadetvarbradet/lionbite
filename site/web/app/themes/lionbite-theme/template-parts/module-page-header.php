<header class="module--page-header">
	<div class="flex-video widescreen">
		<div id="player"></div>
		<div class="bg" style="background-image: url('<?php bloginfo('template_directory') ?>/assets/images/fallback.jpg')">
		</div>
		<div class="slideshow" id="mydiv" background-image: url('<?php bloginfo('template_directory') ?>/assets/images/fallback.jpg')">
			<canvas width="1" height="1" id="container" style="position:absolute"></canvas>
		</div>
		<div class="content">
			<div class="intro">
				<a id="play">
					<img src="<?php bloginfo('template_directory') ?>/assets/images/play.svg">
					<p>View story trailer</p>
				</a>
			</div>
			<img class="down" src="<?php bloginfo('template_directory') ?>/assets/images/down.svg">
		</div>
		<script type="text/javascript">
			var surl = "<?php bloginfo('template_directory') ?>";
			var dynamicWidth = window.innerWidth;
			var dynamicHeight = document.getElementById('mydiv').offsetHeight;
			var ratioMainImg = (window.innerWidth) / (document.getElementById('mydiv').offsetHeight);
			$(document).ready(function()
			{
			   var resizeDelay = 200;
			   var doResize = true;
			   var resizer = function () {
			      if (doResize) {
			        dynamicWidth = window.innerWidth;
			        dynamicHeight = document.getElementById('mydiv').offsetHeight;
			        $("#container").width(window.innerWidth);
			        $("#container").height(document.getElementById('mydiv').offsetHeight);
			      }
			    };
			    var resizerInterval = setInterval(resizer, resizeDelay);
			    resizer();

			    $(window).resize(function() {
			      doResize = true;
			    });
			});
		</script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/javascript/vendor/raineffect/raineffect.js"></script>
	</div>
</header>
