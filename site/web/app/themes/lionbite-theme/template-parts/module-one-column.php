<section class="module--one-column">
	<div class="section-content entry-content">
		<?php if( get_sub_field('headline') ): ?>
		<h2><?php the_sub_field('headline'); ?></h2>
		<?php endif; ?>
		<?php the_sub_field('content'); ?>
	</div>
</section>