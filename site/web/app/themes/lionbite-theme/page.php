<?php
/**
 * The template for displaying pages/modules
 *
 * @package lionbite
 * @since lionbite 1.0.0
 */

 get_header(); ?>

 <div id="page" class="row expanded" role="main">

 <?php do_action( 'lionbite_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?>

   <?php if( have_rows('modules') ):
    // loop through the rows of data
    while ( have_rows('modules') ) : the_row();
      // hero
      if( get_row_layout() == 'page_header' ):
        get_template_part( 'template-parts/module-page-header' );
      // 1 column
      elseif( get_row_layout() == 'one_column' ):
        get_template_part( 'template-parts/module-one-column' );
      // 2 columns
      elseif( get_row_layout() == 'two_columns' ):
        get_template_part( 'template-parts/module-two-columns' );
      // services
      elseif( get_row_layout() == 'consultant_card' ):
        get_template_part( 'template-parts/module-consultant-cards' );
      // intro
      elseif( get_row_layout() == 'intro' ):
        get_template_part( 'template-parts/module-intro' );
      // team
      elseif( get_row_layout() == 'team' ):
        get_template_part( 'template-parts/module-team' );
      // seperator
      elseif( get_row_layout() == 'seperator' ):
        get_template_part( 'template-parts/module-seperator' );
      // newsletter
      elseif( get_row_layout() == 'newsletter' ):
        get_template_part( 'template-parts/module-newsletter' );
      // work at lionbite
      elseif( get_row_layout() == 'work' ):
        get_template_part( 'template-parts/module-work' );
      // quote
      elseif( get_row_layout() == 'quote' ):
        get_template_part( 'template-parts/module-quote' );
      // video
      elseif( get_row_layout() == 'video' ):
        get_template_part( 'template-parts/module-video' );
      // core values
      elseif( get_row_layout() == 'core_values' ):
        get_template_part( 'template-parts/module-core-values' );
      // bullets
      elseif( get_row_layout() == 'bullets' ):
        get_template_part( 'template-parts/module-bullets' );
      // blog
      elseif( get_row_layout() == 'blog' ):
        get_template_part( 'template-parts/module-blog' );
      // clients
      elseif( get_row_layout() == 'clients' ):
        get_template_part( 'template-parts/module-clients' );
      // colored box
      elseif( get_row_layout() == 'colored_box' ):
        get_template_part( 'template-parts/module-colored-box' );
      // question and answers
      elseif( get_row_layout() == 'question_and_answers' ):
        get_template_part( 'template-parts/module-questions-and-answers' );

      elseif( get_row_layout() == 'content' ):
        get_template_part( 'template-parts/module-content' );

      elseif( get_row_layout() == 'ror' ):
        get_template_part( 'template-parts/module-ror' );

      elseif( get_row_layout() == 'recruitment' ):
        get_template_part( 'template-parts/module-recruitment' );

      elseif( get_row_layout() == 'image' ):
        get_template_part( 'template-parts/module-image' );

      elseif( get_row_layout() == 'rain_header' ):
        get_template_part( 'template-parts/module-page-header' );

      endif;
    endwhile;
  else :
endif; ?>

 <?php endwhile;?>

 <?php do_action( 'lionbite_after_content' ); ?>
 </div>

 <?php get_footer();
