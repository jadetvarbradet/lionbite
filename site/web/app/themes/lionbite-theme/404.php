<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package Lionbite
 * @since Lionbite 1.0.0
 */

get_header(); ?>

<div class="404 row" style="height: 100vh;width: 100vw;">
	<div class="small-12 large-8 columns" role="main">

		<article style="position: absolute; top:50%;left:50%;text-align: center;transform:translate(-50%,-70%)" <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header>
			<img style="width:150px" id="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/lb-gold.svg">
				<h1 style="margin-top: 15px; white-space: nowrap;" class="entry-title"><?php _e( 'Page not found', 'foundationpress' ); ?></h1>
			</header>
			<div class="entry-content">
					<p style="white-space: nowrap;"><?php printf( __( 'Return to the <a href="%s">home page</a>', 'foundationpress' ), home_url() ); ?></li>
				</p>
			</div>
		</article>

	</div>
</div>
<?php get_footer();
