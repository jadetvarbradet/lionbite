<?php
/**
 * Add .button class to next/previous links.
 *
 * @package lionbite
 * @since lionbite 1.0.0
 */

add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');

function posts_link_attributes() {
    return 'class="button"';
}