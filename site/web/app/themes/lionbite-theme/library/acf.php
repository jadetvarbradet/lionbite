<?php
/**
 * ACF options
 *
 * @package lionbite
 * @since lionbite 1.0.0
 */

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Options',
		'menu_title'	=> 'Options',
		'menu_slug' 	=> 'lb-extra-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}


function my_acf_format_value( $value, $post_id, $field ) {
	// run do_shortcode on all textarea values
	$value = do_shortcode($value);
	// return
	return $value;
}

add_filter('acf/format_value/type=textarea', 'my_acf_format_value', 10, 3);

?>