<section class="module--ror" role="banner" style="background-image: url('<?php bloginfo('template_directory') ?>/assets/images/fallback.jpg');">
	<div class="section-content" >
		<div class="container">
				<div class="content">
					<div class="image">
						<figure>
							<a href="<?php the_sub_field('button_link'); ?>">
								<img class="herologo" src="<?php bloginfo('template_directory') ?>/assets/images/ror-logo-white.svg">
							</a>
						</figure>
					</div>
					<div class="text">
						<?php if( get_sub_field('headline') ): ?>
							<a href="<?php the_sub_field('button_link'); ?>">
								<h2><?php the_sub_field('headline'); ?></h2>
							</a>
						<?php endif; ?>
						<?php if( get_sub_field('text') ): ?>
							<p><?php the_sub_field('text'); ?></p>
						<?php endif; ?>
						<?php if( get_sub_field('button_text') ): ?>
							<a class="ror_button" href="<?php the_sub_field('button_link'); ?>"><?php the_sub_field('button_text'); ?></a>
						<?php endif; ?>
					</div>
			</div>
			<div class="slideshow" id="mydiv">
				<canvas width="1" height="1" id="container" style="position:absolute"></canvas>
			</div>
		</div>
		<script type="text/javascript">
			var surl = "<?php bloginfo('template_directory') ?>";
			var dynamicWidth = window.innerWidth;
			var dynamicHeight = document.getElementById('mydiv').offsetHeight;
			var ratioMainImg = (window.innerWidth) / (document.getElementById('mydiv').offsetHeight);
			$(document).ready(function()
			{
			   var resizeDelay = 200;
			   var doResize = true;
			   var resizer = function () {
			      if (doResize) {
			        dynamicWidth = window.innerWidth;
			        dynamicHeight = document.getElementById('mydiv').offsetHeight;
			        $("#container").width(window.innerWidth);
			        $("#container").height(document.getElementById('mydiv').offsetHeight);
			      }
			    };
			    var resizerInterval = setInterval(resizer, resizeDelay);
			    resizer();

			    $(window).resize(function() {
			      doResize = true;
			    });
			});
		</script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/javascript/vendor/raineffect/raineffect.js"></script>
	</div>
</section>