<?php
/**
 * All shortcodes goes here.
 *
 * @package lionbite
 * @since lionbite 1.0.0
 */

// Add Shortcode
function button_shortcode( $atts, $content = null ) {

	// Attributes
	$atts = shortcode_atts(
		array(
			'url' => '#',
			'color' => '',
			'open' => '',
			'select' => '',
		),
		$atts
	);
	if (!empty($atts['open'])) {
		$modal = 'data-open="'.$atts['open'].'"';
	} else {
		$modal = '';
	}
	if (!empty($atts['select'])) {
		$select = ' data-select="'.$atts['select'].'"';
		$id = 'id="ask-the-experts" ';
	} else {
		$select = '';
		$id = '';
	}
	return '<a ' . $id . 'href="'.$atts['url'].'" class="' . $atts['color'] . ' button"' . $modal . $select .'">' . $content . '</a>';
}
add_shortcode( 'button', 'button_shortcode' );