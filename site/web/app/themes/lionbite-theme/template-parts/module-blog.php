<section class="module--blog">
		<?php
		// check for category
		if(get_sub_field('title')) {
			$title = get_sub_field('title');
		} else {
			$title = 'lionbite bloggen';
		} ?>
	<h3 class="section-title"><?php echo $title; ?></h3>
	<div class="grid row small-up-1 medium-up-2 large-up-3">
		<?php
		// check for category
		if(get_sub_field('category')) {
			$catgeory = get_sub_field('category');
		} else {
			$catgeory = 0;
		}
		$args = array( 'posts_per_page' => 3, 'cat' => $catgeory );
		$blog_query = new WP_Query( $args );

		if ( $blog_query->have_posts() ) :
		    while ( $blog_query->have_posts() ) : $blog_query->the_post();
				get_template_part( 'template-parts/content', get_post_format() );
		    endwhile;
		else:
		    _e( 'Sorry, no posts matched your criteria.', 'lionbite' );
		endif;
		wp_reset_postdata();
		?>
	</div>
</section>