<section class="module--clients">
	<div class="section-content">
		<div class="logo-wrap">
			<h3 class="section-title">Några av våra kunder</h3>
			<div class="grid row small-up-2 medium-up-4 large-up-5">
				<?php
				$clients = get_sub_field('clients');
				if ( $clients ) :
				    foreach( $clients as $client ): ?>
					<?php $image = wp_get_attachment_image_src(get_field('logotype', $client->ID), 'lionbite-240w'); ?>
					<div class="client column">
						<img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title( $client->ID ); ?>" />
					</div>
				<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="button-wrap">
		<?php if (get_sub_field('button_label')) { ?>
			<a class="button orange" data-open="call-me"><?php the_sub_field('button_label'); ?></a>
		<?php } ?>
	</div>
</section>