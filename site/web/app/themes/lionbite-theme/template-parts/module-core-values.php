<section class="module--core-values">
	<div class="section-content">
		<h3 class="section-title"><?php the_field('values_title','option'); ?></h3>
		<div class="orbit" role="region" aria-label="Core Values" data-orbit data-options="animInFromLeft:fade-in; animInFromRight:fade-in; animOutToLeft:fade-out; animOutToRight:fade-out;">
			<ul class="orbit-container">
				<button class="orbit-previous"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
    			<button class="orbit-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
				<?php
				// check if the repeater field has rows of data
				if( have_rows('values', 'option') ):
				$count = 0;
			 	// loop through the rows of data
			    while ( have_rows('values', 'option') ) : the_row(); ?>
				<li class="<?php if (!$count) { ?>is-active<?php } ?> orbit-slide">
					<h2 class="h1"><?php the_sub_field('title'); ?></h2>
					<p><?php the_sub_field('content'); ?></p>
				</li>
				<?php
				$count++;
				endwhile;
				else :
				// no rows found
				endif;
				?>
			</ul>
			  <nav class="orbit-bullets">
			  <?php
				// check if the repeater field has rows of data
				if( have_rows('values', 'option') ):
				$count = 0;
			 	// loop through the rows of data
			    while ( have_rows('values', 'option') ) : the_row(); ?>
			    	<button <?php if (!$count) { ?>class="is-active"<?php } ?> data-slide="<?php echo $count; ?>"></button>
				<?php
				$count++;
				endwhile;
				else :
				// no rows found
				endif;
				?>
			  </nav>
		</div>
	</div>
</section>