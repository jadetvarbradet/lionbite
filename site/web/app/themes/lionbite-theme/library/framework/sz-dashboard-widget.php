<?php
/**
 * SZ Dashboard widget
 *
 * @package lionbite
 * @since lionbite 1.0.0
 */
add_action('wp_dashboard_setup', 'sz_dashboard_widgets');

function sz_dashboard_widgets() {
	global $wp_meta_boxes;
	wp_add_dashboard_widget('custom_help_widget', 'Sandström / Zethelius Support', 'custom_dashboard_help');
}

function custom_dashboard_help() {
echo '<p>Need help with your website? <strong>Contact our support at <a href="mailto:support@s-z.se">support@s-z.se</a></strong> to create a ticket. Tickets are normally answered within 24 hours.</p>
<p>For more information and news about Sandström / Zethelius <a href="http://www.s-z.se" target="_blank">click here</a></p>';
}