<section class="module--quote">
	<blockquote>
		<?php the_sub_field('content'); ?>
		<div class="byline">
			<div class="avatar">
				<?php $image = wp_get_attachment_image_src(get_sub_field('image'), 'lionbite-icon'); ?>
				<img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(get_sub_field('image')) ?>" />
			</div>
			<div class="info">
				<span><?php the_sub_field('name'); ?></span><br>
				<?php the_sub_field('company'); ?> - <?php the_sub_field('position'); ?>
			</div>
		</div>
	</blockquote>
</section>