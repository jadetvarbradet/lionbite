<section class="module--bullets">
	<div class="section-content">
		<div class="row small-up-1 medium-up-2 large-up-3">
			<?php
			if( have_rows('bullet') ):
			    while ( have_rows('bullet') ) : the_row(); ?>
				<div class="column">
					<div class="bullet">
						<?php $image = wp_get_attachment_image_src(get_sub_field('image'), 'lionbite-small'); ?>
						<figure>
							<img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(get_sub_field('image')) ?>" />
						</figure>
						<div class="content">
							<?php the_sub_field('content'); ?>
						</div>
			        </div>
			    </div>
			    <?php endwhile;
			else :
			endif;
			?>
		</div>
	</div>
</section>