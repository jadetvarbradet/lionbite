<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

		<article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
			<header>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<?php
					if ( has_post_thumbnail() ) :
						echo '<figure class="featured-image">';
						the_post_thumbnail('fp-large');
						echo '</figure>';
					endif;
				?>
			</header>
			</a>
			<?php do_action( 'foundationpress_post_before_entry_content' ); ?>
			<div class="entry-content">
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</a>
				<?php foundationpress_entry_meta(); ?>
				<?php the_content(); ?>
			</div>
			<footer>
				<?php wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
				<p><?php the_tags(); ?></p>
			</footer>
		</article>
