$('.toggle').click(function(e) {
    // Prevent them from actually visiting the URL when clicking.
    e.preventDefault();
    $(this).closest( '.qa-card' ).toggleClass( 'open' );
    $('.grid').masonry();
});
