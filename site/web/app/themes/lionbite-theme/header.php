<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package Lionbite
 * @since Lionbite 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<?php wp_head(); ?>
	</head>
	<?php $blog_id = get_current_blog_id();
	if ($blog_id == "1") :
		$blog_id = "one";
	elseif ($blog_id == "2") :
		$blog_id = "two";
	endif;
	?>

	<?php if ((is_archive() || is_single() || is_page(103) || is_page(246)) && ($blog_id == "two")):
		if(get_field('background_image') ):
			$background = get_field('background_image');
			$bg_size = 'rgba(255, 255, 255, 0));background-size:cover; background-position: center;  background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 1)),';
		else:
			$background = get_stylesheet_directory_uri() . '/assets/images/blog-bg.jpg';
			$bg_size = 'rgba(255, 255, 255, 0));background-size:cover; background-position: center;  background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 1)),';
		endif;
	else:
		$bg_size = '';
		$background = 'none';
	endif;
	// var_dump($background)
	?>

	<body id="<?php echo $blog_id; ?>" <?php body_class(); ?>>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=115510609039962";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

	<div style="<?php echo $bg_size ?> url(<?php echo $background; ?>)" class="background-container"></div>

	<div class="body-container">

	<?php do_action( 'foundationpress_after_body' ); ?>

	<?php do_action( 'foundationpress_layout_start' ); ?>

	<header id="masthead" class="site-header" role="banner">

		<nav id="site-navigation" class="main-navigation top-bar" role="navigation">
			<div class="top-bar-left">
				<ul id="menu" class="menu">
					<li class="home"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
					<img id="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/ror-logo-white.svg"></a></li>
				</ul>
			</div>
			<div class="top-bar-right">
				<?php foundationpress_top_bar_r(); ?>
				<div class="lb">
					<a href="http://lionbite.games">
					<svg class="show-for-medium" viewBox="0 0 458 123" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><title>lb-black</title><defs><path id="a" d="M0 123h457.492V.25H0z"/></defs><g fill="none" fill-rule="evenodd"><path fill="#000" d="M233.372 49.102h13.968l-6.984-6.984zM208.827 49.102h13.968l-6.984-6.984z"/><path d="M228.083 0l-51.638 51.64 9.735 9.737-9.735 9.734 51.638 51.64 51.637-51.64-9.734-9.733 9.734-9.736L228.083 0zm-29.63 73.648L186.18 61.376 198.453 49.1V31.746h17.357l12.273-12.273 12.273 12.273h17.357V49.1l12.273 12.276-12.273 12.272-.298.298-6.983-6.984-1.398 1.397 6.984 6.983-6.285 6.285-6.984-6.984-1.397 1.397 6.983 6.985-6.285 6.286-12.57-12.57-.41-.41V61.376h5.996l-6.983-6.984-6.984 6.984h5.995V76.33l-.41.41-12.57 12.57-6.286-6.285 6.984-6.984-1.397-1.396-6.984 6.984-6.286-6.285 6.984-6.984-1.395-1.398-6.984 6.984-.297-.298zm17.357 17.357l-.298-.297 12.57-12.572 12.572 12.572-.298.297-12.273 12.273-12.273-12.273zM339.81 83.46h7.032V39.29h-7.033zM398.045 39.29h-44.17v7.033h18.568V83.46h7.034V46.323h18.568zM450.558 39.29h-45.48v44.17h44.171v-7.033H412.11V64.89h22.084v-7.032h-22.084V46.322h38.446v-.1l6.935-6.933z" fill="#000"/><g transform="translate(0 -.25)"><path d="M323.656 74.76c1.333-1.28 2-2.42 2-4.736 0-2.28-.667-4.05-2-5.315-1.333-1.263-3.213-1.893-5.64-1.893H295.64v13.86h22.377c2.426 0 4.306-.64 5.64-1.918zM295.64 46.57v9.21h20.144c2.216 0 3.888-.44 5.014-1.322 1.126-.88 1.69-1.63 1.69-3.36 0-1.726-.564-2.368-1.69-3.234-1.126-.863-2.83-1.294-5.118-1.294h-20.04zm35.265 17.15c1.247 1.99 1.872 4.23 1.872 6.718 0 4.112-1.38 7.353-4.13 9.718-2.756 2.37-6.56 3.552-11.406 3.552h-28.633V46.475l-6.934-6.934h34.214c4.503 0 7.915.927 10.238 2.774 2.32 1.85 3.482 4.572 3.482 8.166 0 1.797-.364 3.394-1.092 4.794-.726 1.4-1.786 2.53-3.17 3.397 2.46 1.38 4.313 3.067 5.56 5.053z" fill="#000"/><mask id="b" fill="#fff"><use xlink:href="#a"/></mask><path fill="#000" mask="url(#b)" d="M58.138 83.71h7.033V39.54H58.14zM94.29 76.445c-8.186 0-14.82-6.637-14.82-14.82 0-8.184 6.634-14.82 14.82-14.82 8.183 0 14.817 6.636 14.817 14.82 0 8.183-6.634 14.82-14.818 14.82m-.002-36.904c-12.196 0-22.085 9.887-22.085 22.085 0 12.196 9.89 22.085 22.085 22.085 12.197 0 22.085-9.89 22.085-22.085 0-12.198-9.888-22.084-22.085-22.084M167.634 39.54H160.6v28.95l-28.95-28.95h-8.244V83.712h7.033V48.277l35.434 35.435h9.946l-8.186-8.186zM13.968 76.677V39.54H0l6.935 6.935V83.71h44.17v-7.033z"/></g></g></svg>
					<svg class="hide-for-medium" width="104" height="123" viewBox="0 0 104 123" xmlns="http://www.w3.org/2000/svg"><title>lb-icon-black</title><g fill="#000" fill-rule="evenodd"><path d="M57 48.984h13.968L63.984 42zM32 48.984h13.968L38.984 42z"/><path d="M51.638 0L0 51.64l9.735 9.736L0 71.11l51.638 51.638 51.637-51.638-9.734-9.734 9.735-9.736L51.638 0zm-29.63 73.647L9.735 61.375 22.008 49.1V31.744h17.357L51.638 19.47 63.91 31.745H81.27V49.1L93.54 61.375 81.27 73.647l-.298.298-6.983-6.984-1.398 1.398 6.983 6.984-6.285 6.285-6.984-6.984-1.396 1.397 6.983 6.984-6.284 6.286-12.57-12.57-.41-.41V61.374h5.995L51.64 54.39l-6.984 6.985h5.996V76.33l-.41.41-12.57 12.57-6.286-6.286 6.985-6.984-1.398-1.397-6.984 6.984-6.286-6.285 6.984-6.984-1.396-1.397-6.984 6.985-.298-.298zm17.357 17.357l-.298-.297 12.57-12.572L64.21 90.707l-.3.297-12.272 12.273-12.273-12.273z"/></g></svg>
					</a>
				</div>
			</div>
			<div class="h-container" onclick="hamburgerEvent()" id="nav-icon3">
			  <span></span>
			  <span></span>
			  <span></span>
			  <span></span>
			</div>
		</nav>
	</header>



	<section class="container">
		<?php do_action( 'foundationpress_after_header' );
