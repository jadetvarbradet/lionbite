<?php
/**
 * Custom Dashboard Logo
 *
 * @package lionbite
 * @since lionbite 1.0.0
 */

function lionbite_login_logo() {
	echo '<style type="text/css">
	h1 a { background-image:url('.get_bloginfo('template_directory').'/assets/images/lb-black.svg) !important;
	background-size: 280px !important;
	height: 100px !important;
	width: 280px !important; }
	</style>';
}
add_action('login_head', 'lionbite_login_logo');

// Links logo in Dashboard to home url
function lionbite_url_login() {
	return get_bloginfo('url');
}
add_filter('login_headerurl', 'lionbite_url_login');