<?php
/**
 * Custom post types goes here.
 *
 * @package lionbite
 * @since lionbite 1.0.0
 */

// Register Custom Post Type

// Register Custom Post Type
function team() {

	$labels = array(
		'name'                  => _x( 'Team Members', 'Post Type General Name', 'lionbite' ),
		'singular_name'         => _x( 'Team', 'Post Type Singular Name', 'lionbite' ),
		'menu_name'             => __( 'Team', 'lionbite' ),
		'name_admin_bar'        => __( 'Team', 'lionbite' ),
		'archives'              => __( 'Team Member Archives', 'lionbite' ),
		'parent_item_colon'     => __( 'Parent Team Member:', 'lionbite' ),
		'all_items'             => __( 'All Team Members', 'lionbite' ),
		'add_new_item'          => __( 'Add Team Member', 'lionbite' ),
		'add_new'               => __( 'Add New Member', 'lionbite' ),
		'new_item'              => __( 'New Team Member', 'lionbite' ),
		'edit_item'             => __( 'Edit Team Member', 'lionbite' ),
		'update_item'           => __( 'Update Team Member', 'lionbite' ),
		'view_item'             => __( 'View Team Member', 'lionbite' ),
		'search_items'          => __( 'Search Team Members', 'lionbite' ),
		'not_found'             => __( 'Not found', 'lionbite' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'lionbite' ),
		'featured_image'        => __( 'Team Member Image', 'lionbite' ),
		'set_featured_image'    => __( 'Set Team Member image', 'lionbite' ),
		'remove_featured_image' => __( 'Remove Team Member image', 'lionbite' ),
		'use_featured_image'    => __( 'Use as Team Member image', 'lionbite' ),
		'insert_into_item'      => __( 'Insert into item', 'lionbite' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'lionbite' ),
		'items_list'            => __( 'Items list', 'lionbite' ),
		'items_list_navigation' => __( 'Items list navigation', 'lionbite' ),
		'filter_items_list'     => __( 'Filter items list', 'lionbite' ),
	);
	$args = array(
		'label'                 => __( 'Team Member', 'lionbite' ),
		'description'           => __( 'Team Members post type.', 'lionbite' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', 'custom-fields' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
		'menu_icon'             => 'dashicons-businessman',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => false,
		'capability_type'       => 'post',
	);
	register_post_type( 'team', $args );

}
add_action( 'init', 'team', 0 );