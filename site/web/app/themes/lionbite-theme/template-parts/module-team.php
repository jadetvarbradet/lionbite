<section class="module--team">
	<div class="section-content">
		<h2 class="section-title">The Team</h1>
		<div class="grid row small-up-2 medium-up-3 large-up-4">
		<?php
		$team_members = get_sub_field('team_members');
		if($team_members) {
			foreach($team_members as $team_member)
			{ ?>
			<div class="column card-wrap">
				<div class="team-member-card">
					<?php // Get data
					$description = get_field('description', $team_member->ID);
					$title = get_field('title', $team_member->ID);
					$email = get_field('email', $team_member->ID);
					?>
					<figure>

					<?php
						if ( has_post_thumbnail($team_member->ID) ) :
							echo get_the_post_thumbnail($team_member->ID, 'lionbite-team');
						endif;
					?>
					<div class="overlay"><p><?php echo $description; ?></p></div>
					</figure>
					<div class="contact-information">
						<h3><?php echo get_the_title($team_member->ID); ?></h3>
						<?php echo $title; ?>
						<br />
						<a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
					</div>
				</div>
			</div>
			<?php
			}
		}
		?>
		</div>
	</div>
</section>
