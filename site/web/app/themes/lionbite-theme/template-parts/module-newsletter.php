<section class="module--newsletter">
	<div class="section-content">
		<div class="newsletter">
		<script src="https://apis.google.com/js/platform.js"></script>
		<?php $type = 'Consumer' ?>
			<?php echo do_shortcode('[mc4wp_form id="219"]'); ?>
		</div>
		<div class="follow">
			<?php echo do_shortcode('[twitter_follow screen_name="LionbiteGames" size="large" show_count="false"]'); ?>
			<div class="fb-follow" data-href="https://www.facebook.com/lionbitegames" data-layout="button" data-size="large" data-show-faces="true"></div>
			<div class="yt-follow">
				<div class="g-ytsubscribe" data-channel="lionbitegames" data-layout="default" data-theme="dark" data-count="hidden"></div>
			</div>
		</div>
	</div>
</section>