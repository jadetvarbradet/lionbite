<?php
	$img = get_sub_field('background');
	$alt = $img['alt'];
	$size = 'fp-large';
	$background = $img['sizes'][ $size ];
	if ( get_sub_field('switch') == 'switch' ) :
	$switch = get_sub_field('switch');	
	endif;
?>
<section class="module--content" role="banner" style="background-image: url('<?php echo $background; ?>');">
	<aside class="grain"></aside>
	<div class="container">
		<div class="text <?php echo $switch; ?>">
			<?php the_sub_field('text'); ?>
		</div>
		<div class="image <?php echo $switch; ?>">
			<figure>
				<img src="<?php the_sub_field('image'); ?>" alt="<?php echo get_the_title(get_sub_field('image')) ?>" />
			</figure>
		</div>
	</div>
</section>