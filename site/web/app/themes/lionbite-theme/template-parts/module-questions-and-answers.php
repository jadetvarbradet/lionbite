<section class="module--questions-and-answers">
	<header>
		<?php
			if ( has_post_thumbnail() ) :
				echo '<figure class="featured-image">';
				the_post_thumbnail();
				echo '</figure>';
			endif;
		?>
	</header>
	<div class="questions">
	<h1><?php the_title(); ?></h1>
		<?php
		if( have_rows('question_and_answer') ):
		    while ( have_rows('question_and_answer') ) : the_row(); ?>
				<div class="question">
					<?php if( get_sub_field('title') ): ?>
					<h4 class="dot-title"><?php the_sub_field('title'); ?></h4>
					<?php endif; ?>
					<?php the_sub_field('answer'); ?>
		    	</div>
		    <?php endwhile;
		else :
		endif;
		?>
	</div>
</section>