<section class="module--two-columns">
	<div class="left-column">
		<?php the_sub_field('column_1'); ?>
	</div>
	<div class="right-column">
		<?php the_sub_field('column_2'); ?>
	</div>
</section>