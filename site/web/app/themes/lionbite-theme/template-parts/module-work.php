<section class="module--work">
	<div class="section-content" >
		<div class="banner" style="background-color:<?php the_field('background_color', 'option'); ?>" data-equalizer data-equalize-on="medium" id="banner">
			<div class="content" data-equalizer-watch>
				<?php the_field('content', 'option'); ?>
			</div>
			<div class="cta" data-equalizer-watch>
				<a href="<?php the_field('button_url', 'option'); ?>" class="button"><?php the_field('button_label', 'option'); ?></a>
			</div>
		</div>
	</div>
</section>