<section class="module--colored-box">
	<?php
	$number_of_fields = count( get_sub_field('colored_box') );
	// check if the repeater field has rows of data
	if( have_rows('colored_box') ): ?>
	<div class="section-content" <?php if ($number_of_fields == '2') { ?>data-equalizer data-equalize-on="large"<?php } ?>>
 	<?php // loop through the rows of data
    while ( have_rows('colored_box') ) : the_row(); ?>
    <div class="column small-12 <?php if ($number_of_fields == '2') { ?>large-6<?php } ?>">
    	<div class="box<?php if ($number_of_fields == '2') { ?> boxes<?php } ?> <?php if ($number_of_fields == '1' && is_front_page()) { ?>buzzwords<?php } ?>" style="background-color: <?php the_sub_field('color'); ?> " <?php if ($number_of_fields == '2') { ?>data-equalizer-watch<?php } ?>>
    		<div class="box-content">
				<h2 class="h1"><?php the_sub_field('title'); ?></h2>
				<p><?php the_sub_field('content'); ?></p>
			</div>
			<?php
			if (get_sub_field('image')) {
			$image = wp_get_attachment_image_src(get_sub_field('image'), 'lionbite-team'); ?>
			<figure>
				<img src="<?php echo $image[0]; ?>" />
			</figure>
			<?php } ?>
			<?php if ($number_of_fields == '1' && is_front_page()) { ?>
			<div class="buzzword">
			<?php
			$rows = get_field('words','options');
 			if($rows) $i = 0; {
			shuffle( $rows );
			foreach($rows as $row) {
      		$i++; if($i==2) break;
      		$word = $row['word'];
      		$explanation = $row['explanation']; ?>
      			<h2>Dagens BI-Buzzword!</h2>
				<h3><?php echo $word; ?></h3>
				<p><?php echo $explanation; ?></p>
			<?php }
			} ?>
			</div>
		<?php } ?>
		</div>
	</div>
	<?php
	endwhile; ?>
	<?php
	else :
	endif;
	?>
	</div>
</section>